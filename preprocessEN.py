
import re, string

import nltk
from nltk.stem import WordNetLemmatizer 
nltk.download('averaged_perceptron_tagger') 
from nltk.corpus import wordnet 


def clean_text(text):       
    #Tokenize the data
    text = nltk.word_tokenize(text)
    #Remove stopwords
    text = [w for w in text if w not in sw]    
    return text


#Lemmatizer
lemmatizer = WordNetLemmatizer()


def get_wordnet_pos(word):    
    tag = nltk.pos_tag([word])[0][1][0]
    print(tag)

    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    if tag in tag_dict:
         return tag_dict.get(tag)
    else:
        return None
        

def lem(sentence):
        
    pos_tagged = [(w , get_wordnet_pos(w)) for w in nltk.word_tokenize(sentence)] 

    lemmatized_sentence = [] 

    for word, tag in pos_tagged:       
        if tag is None: 
            continue
        else:                     
            lemmatized_sentence.append((lemmatizer.lemmatize(word, tag), tag))     

    return lemmatized_sentence




