import resources
import os
from os.path import isfile, join

class Classified:    

    dict_opinion = {}   
    negation = []
    modifiers = []
    lang = ''

    def __init__(self, lang):
        self.lang = lang

        script_path = os.path.abspath(__file__) 
        script_dir = os.path.split(script_path)[0]

        self.dict_opinion = resources.leer_SWN(os.path.join(script_dir, "recursos/" + self.lang + "/SWN_spa.txt"))

        self.negation = resources.leer_negadores(os.path.join(script_dir, "recursos/" + self.lang + "/negation.txt"))

        self.modifiers = resources.leer_modificadores(os.path.join(script_dir, "recursos/" + self.lang + "/modifiers.txt"))

    

    #reglas para classificar
    def getpolarity(self, vector):
        
        polarity = 0
        
        
        for i in range(len(vector)):
              if i > 0 and vector[i-1][0] in self.negation:
                  neg = -1        
              else:
                  neg = 1    
              if i > 0 and vector[i-1][0] in self.modifiers:
                  mod = self.modifiers.get(vector[i-1][0])                
              else:
                  mod = 1

              if vector[i][1] in self.dict_opinion and vector[i][0] in self.dict_opinion[vector[i][1]]['P'] :                 
                  polarity += 2 * neg * mod
                  #print(vector[i][0] , 'P')
              elif vector[i][1] in self.dict_opinion and vector[i][0] in self.dict_opinion[vector[i][1]]['HP'] :
                  polarity += 4 * neg * mod
                  #print(vector[i][0] , 'HP')
              elif vector[i][1] in self.dict_opinion and vector[i][0] in self.dict_opinion[vector[i][1]]['HN'] :   
                  polarity += -4 * neg * mod
                  #print(vector[i][0] , 'HN')
              elif vector[i][1] in self.dict_opinion and vector[i][0] in self.dict_opinion[vector[i][1]]['N'] :
                  polarity += -2 * neg * mod
                  #print(vector[i][0] , 'N')
              else:
                  continue

        if polarity > 2 :
            return 'POS'            
        elif polarity < -2:            
            return 'NEG'
        else:
            return 'OBJ'    





