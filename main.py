import preprocessSP
import os
from os.path import isfile, join
import collections
import pandas as pd
import classifier 

def readFile(fileDir):
    try:       
        lines = open(fileDir,'r')

        result = {}

        while True:            
    
            line = lines.readline() 
    
            if not line: 
                break
            dupla = str.split(line, '\t')        
            result[int(dupla[0])] = dupla[1]

        return result        
        
               
    except   (IOError):    
        print('No se leyó el fichero')

def readFilePanda(fileDir):
    return pd.read_csv(fileDir, header=0, delimiter="\t")  


if __name__ == '__main__':

    lang = input('Idioma del recurso[es/en]:')
    
    script_path = os.path.abspath(__file__) 
    script_dir = os.path.split(script_path)[0] 
    rel_path = "recursos/" + lang + "/sentences.csv"
    abs_file_path = os.path.join(script_dir, rel_path)  

    if lang == 'en':

        df = readFilePanda(abs_file_path)

        df['lemma'] = df['comment'].apply(lambda x: preprocess.lem(x))

        for i in range(len(df)):
            print(df['comment'][i])
            if len(df['comment'][i]) == 0:
                df=df.drop(i)       
        
        df=df.reset_index(drop=True) 

    elif lang == 'es':
           
        cl = classifier.Classified(lang)

        df = readFilePanda(abs_file_path)
        
        df['lemma'] = df['comment'].apply(lambda x: preprocessSP.part_of_speech(x))

        print('Iniciado Clasificador')
        #
        for i in range(len(df['lemma'])):
            print(cl.getpolarity(df['lemma'][i][0]), df['comment'][i])
        
    else:
        print('Idioma no soportado')    

    


       
