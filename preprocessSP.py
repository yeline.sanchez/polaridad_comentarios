import stanza
#stanza.download('es') # download Spanish model

config = {    
    'processors': 'tokenize,pos,lemma,ner',
    'lang': 'es',
}

nlp = stanza.Pipeline(**config) 

pos_tags = {'ADJ': 'a', 'ADV': 'r' , 'AUX': 'v', 'INTJ': 'a', 'NOUN': 'n', 'PROPN': 'n', 'VERB': 'v', 'DET': 'r'} 

def part_of_speech(text):
    doc = nlp(text) 

    result = {}  
    s = []  

    [[s.append((word.lemma, pos_tags.get(word.xpos))) for word in sent.words if word.xpos in pos_tags] for sent in doc.sentences ]                   

    result[0] = s
    result[1] = doc.entities            
    return result


       

