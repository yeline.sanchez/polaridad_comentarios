import string
import collections
import math
import os
from os.path import isfile, join


def leer_palabras_pos(fileDir):
                
    try:       
        lines = open(fileDir,'r').read().split('\n')   
        palabras = {}
               
        for line in lines:
            terna = str.split(line, '\t')
            if terna[0] not in palabras:
                palabras[terna[1]] = []
            palabras[terna[0]].append(terna[1])

        return palabras    

    except   (IOError):    
        print('No se leyó el fichero') 
        return None

      

def leer_modificadores(fileDir):
                
    try:       
        lines = open(fileDir,'r').read().split('\n')       
        palabras = {}
               
        for line in lines:
            terna = str.split(line, '\n')
            palabras[terna[0]] = terna[0]

        return palabras   

    except   (IOError):    
        print('No se leyó el fichero') 
        return None   

def leer_negadores(fileDir):
                
    try:       
        lines = open(fileDir,'r').read().split('\n')       
        palabras = []
               
        for line in lines:
            terna = str.split(line, '\n')
            palabras.append(terna[0])

        return palabras   

    except   (IOError):    
        print('No se leyó el fichero') 
        return None             


def leer_frases(fileDir):
    try:       
        lines = open(fileDir,'r').read().split('\n')       
        frases = {}
               
        for line in lines:
            terna = str.split(line, '\t')
            frases[terna[0]] = terna[2]  

        return frases  

    except   (IOError):    
        print('No se leyó el fichero')        
        return None

    

def leer_SWN(fileDir):
    try:       
        lines = open(fileDir,'r').read().split('\n')  

        recursos = {}

        for line in lines:
            terna = str.split(line, '\t')
            if len(terna) > 1:
                if terna[1] not in recursos:
                    recursos[terna[1]] = {'HP': [], 'HN': [], 'N': [], 'P': []}
                else:
                    recursos[terna[1]][terna[2]].append(terna[0])            

        return recursos

    except   (IOError):    
        print('No se leyó el fichero')    
        return None    

def loadR(self, lang):

        script_path = os.path.abspath(__file__) 
        script_dir = os.path.split(script_path)[0]

        self.dict_opinion = {'v': {}, 'n': {}, 'a': {}, 'r': {} }

        P = resources.leer_palabras_pos(os.path.join(script_dir, "recursos/" + self.lang + "/P"))

        for tag in P:
            self.dict_opinion[tag]['P'] = P.get(tag)

        N = resources.leer_palabras_pos(os.path.join(script_dir, "recursos/" + self.lang + "/N"))

        for tag in N:
            self.dict_opinion[tag]['N'] = N.get(tag)

        HP = resources.leer_palabras_pos(os.path.join(script_dir, "recursos/" + self.lang + "/HP"))

        for tag in HP:
            self.dict_opinion[tag]['HP'] = HP.get(tag)

        HN = resources.leer_palabras_pos(os.path.join(script_dir, "recursos/" + self.lang + "/HN"))

        for tag in HN:
            self.dict_opinion[tag]['HN'] = HN.get(tag)

        
    